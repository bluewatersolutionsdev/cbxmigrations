
# Introduction
*Laravel style database migrations for ContentBox.

Migrations are like version control for your database, allowing your team to easily modify and share the application's database schema. If you have ever had to tell a teammate to manually add a column to their local database schema, you've faced the problem that database migrations solve. 

## Sample Migration ##

###### /modules/contentbox/modules_user/YOURMODULENAME/resources/database/migrations/YYYY_MM_DD_HHMMSS_testTable.cfc

~~~~
component {
    function up() {
        queryExecute("
            CREATE TABLE testTable (
                `id` int NOT NULL AUTO_INCREMENT,
                `fname` varchar(255),
                `lname` varchar(255),
                `email` varchar(255),
                PRIMARY KEY (`id`)
            ) COMMENT='';         
        ");
    }

    function down() {
        queryExecute("
           DROP TABLE testTable;
        ");
    }
}
~~~~
#### Notes
I don't recommend adding "DROP TABLE" statements in production.  Data could be lost.  Use at your own risk.

### Attribution ###

This ContentBox module is built from a ColdBox Module created by [Eric Peterson](https://www.forgebox.io/user/eric%40elpete.com) called [cbmigrations](https://www.forgebox.io/view/cbmigrations)



```

                                       
                                                  $7                        
                                                  $$$                         
                                                 .$$$$                        
                                                 :$$$$.                       
                                                 $$$$$                        
                                               .$$$$$$.                       
                                               $$$$$$$..,                     
                                              $$$$$$+  .$                     
                                             $$$$$=    .Z$                    
                                           .$$$$       .$$Z                   
                                         $$$.$$$$$$$$$$$$$+ . ..             
                                      ..$$$$$$$$$$$$$$7~,......,:+$$I         
                                    .$$$$7.  .                                
888      888                      .+$...              888                                      
888      888                     ..                   888                                      
888      888                                          888                                      
88888b.  888 888  888  .d88b.  888  888  888  8888b.  888888 .d88b.  888d888                   
888 "88b 888 888  888 d8P  Y8b 888  888  888     "88b 888   d8P  Y8b 888P"                     
888  888 888 888  888 88888888 888  888  888 .d888888 888   88888888 888                       
888 d88P 888 Y88b 888 Y8b.     Y88b 888 d88P 888  888 Y88b. Y8b.     888                       
88888P"  888  "Y88888  "Y8888   "Y8888888P"  "Y888888  "Y888 "Y8888  888 

             .dP"Y8  dP"Yb  88     88   88 888888 88  dP"Yb  88b 88 .dP"Y8 
             `Ybo." dP   Yb 88     88   88   88   88 dP   Yb 88Yb88 `Ybo." 
             o.`Y8b Yb   dP 88  .o Y8   8P   88   88 Yb   dP 88 Y88 o.`Y8b 
             8bodP'  YbodP  88ood8 `YbodP'   88   88  YbodP  88  Y8 8bodP' 

```


<cfoutput>
<div class="row">
    <div class="col-md-12">
        <h1 class="h1"><i class="fa fa-database"></i> Migrations</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">

                    <table class="sortable table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Module</th>
                        
                        <th></th>
                    </tr>
                    </thead>
                    <cfloop array="#prc.modules#" index="i"> 
                    <cfif #i.getisactive()#>
  						<tr>
  							<td>#i.getName()#</td>
  						
  							<td>
  								#html.href(href="cbadmin.module.cbxmigrations.main.index", queryString="myModule=#i.getName()#", text="<i class='fa fa-database fa-lg'></i> Migrations", class="btn btn-primary btn-sm")#
  							</td>
  						</tr>
  						</cfif>
					</cfloop>
                    
                </table>

            </div>
        </div>
    </div>
</div>        
</cfoutput>

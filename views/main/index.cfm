<cfoutput>
<div class="row">
    <div class="col-md-12">
        <h1 class="h1"><i class="fa fa-database"></i> Migrations :: #rc.myModule#</h1>
    </div>
</div>
    
<div class="row">
   <div class="col-md-4">
       <div class="panel panel-default">
            <div class="panel-body">
                <h3>Migration Table Installed?</h3>
                    <cfif NOT prc.migrationTableInstalled>
                        <p>No.</p>
                        <form action="#event.buildLink( "cbadmin.module.cbxmigrations.main.install" )#" method="POST">
                        <input type="hidden" name="myModule" value="#rc.myModule#" />
                            <button type="submit" class="btn btn-primary btn-sm">Install Migrations Table</button>
                        </form>
                        <br />
                        <form action="#event.buildLink( "cbadmin.module.cbxmigrations.main.install" )#" method="POST">
                            <input type="hidden" name="runAll" value="true" />
                            <input type="hidden" name="myModule" value="#rc.myModule#" />
                            <button type="submit" class="btn btn-primary btn-sm">Install and Run All Migrations</button>
                        </form>
                    <cfelse>
                        <p>Installed and ready to go!</p>
                       <!--- <form action="#event.buildLink( "cbadmin.module.cbxmigrations.main.uninstall" )#" method="POST">
                        <input type="hidden" name="myModule" value="#rc.myModule#" />
                            <button type="submit" class="btn btn-danger btn-sm">
                                Rollback All and Uninstall Migrations Table
                            </button>
                        </form>--->
                    </cfif>
            </div>
        </div>
   </div>
   <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-body">
               <h3>All Migrations for module: #rc.myModule#</h3>
                <p>Available actions</p>
                   <!---<div class="col-md-2">
                        <form action="#event.buildLink( "cbxmigrations.nextUp" )#" method="POST">
                            <button
                                type="submit"
                                class="btn btn-primary btn-sm"
                                <cfif NOT prc.migrationTableInstalled>disabled</cfif>
                            >
                                Run Next Up
                            </button>
                        </form>
                    </div>
                    <div class="col-md-2">
                        <form action="#event.buildLink( "cbxmigrations.nextDown" )#" method="POST">
                            <button
                                type="submit"
                                class="btn btn-danger btn-sm"
                                <cfif NOT prc.migrationTableInstalled>disabled</cfif>
                            >
                                Run Next Down
                            </button>
                        </form>
                    </div>--->
                    <div class="col-md-2">
                        <form action="#event.buildLink( "cbadmin.module.cbxmigrations.main.migrate.direction.up" )#" method="POST">
                            <input type="hidden" name="all" value="true" />
                            <input type="hidden" name="myModule" value="#rc.myModule#" />
                            <button
                                type="submit"
                                class="btn btn-primary btn-sm"
                                <cfif NOT prc.migrationTableInstalled>disabled</cfif>
                            >
                                Run All Remaining
                            </button>
                        </form>
                        </div>
                    <div class="col-md-2">
                        <form action="#event.buildLink( "cbadmin.module.cbxmigrations.main.migrate.direction.down" )#" method="POST">
                            <input type="hidden" name="all" value="true" />
                            <input type="hidden" name="myModule" value="#rc.myModule#" />
                            <button
                                type="submit"
                                class="btn btn-danger btn-sm"
                                <cfif NOT prc.migrationTableInstalled>disabled</cfif>
                            >
                                Rollback All
                            </button>
                        </form>
                        </div>
                    <div class="col-md-2">
                        <form action="#event.buildLink( "cbadmin.module.cbxmigrations.main.refresh" )#" method="POST">
                        <input type="hidden" name="myModule" value="#rc.myModule#" />
                            <button
                                type="submit"
                                class="btn btn-warning btn-sm"
                                <cfif NOT prc.migrationTableInstalled>disabled</cfif>
                            >
                                Refresh All
                            </button>
                        </form>
                    </div> 
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">

                    <table class="sortable table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Migration</th>
                        <th>Migrated?</th>
                        <th colspan="2">Actions</th>
                    </tr>
                    </thead>
                    <cfloop array="#prc.migrations#" index="migration">
                        <tr>
                            <td>#migration.componentName#</td>
                            <td>#migration.migrated ? "<i class='fa fa-check'></i> Yes" : "<i class='fa fa-remove'></i> No"#</td>
                            <td width="100">
                                <div class="btn-group pull-right">     
                                <form action="#event.buildLink( "cbadmin.module.cbxmigrations.main.migrate.direction.up" )#" method="POST">
                                    <input type="hidden" name="componentPath" value="#migration.componentPath#" />
                                    <input type="hidden" name="myModule" value="#rc.myModule#" />
                                    <button
                                        type="submit"
                                        class="btn btn-primary btn-sm"
                                        <cfif NOT migration.canMigrateUp>disabled</cfif>
                                    >
                                        Up
                                    </button>
                                </form>
                                </div>
                            </td>
                            <td width="100">
                             <div class="btn-group pull-right">     
                                <form action="#event.buildLink( "cbadmin.module.cbxmigrations.main.migrate.direction.down" )#" method="POST">
                                   
                                    <input type="hidden" name="componentPath" value="#migration.componentPath#" />
                                    <input type="hidden" name="myModule" value="#rc.myModule#" />
                                    <button
                                        type="submit"
                                        class="btn btn-danger btn-sm"
                                        <cfif NOT migration.canMigrateDown>disabled</cfif>
                                    >
                                        Down
                                    </button>
                                </form>
                                </div>
                            </td>
                        </tr>
                    </cfloop>
                </table>

            </div>
        </div>
    </div>
</div>    
</cfoutput>

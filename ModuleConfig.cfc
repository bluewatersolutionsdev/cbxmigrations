component {
    this.title = "cbxmigrations";
    this.author = "Andrew Davis";
    this.webURL="";
    this.description = "Keep track of database migrations using ContentBox built on cbmigrations from Eric Peterson";
    this.version = "1.0.5";
    // If true, looks for views in the parent first, if not found, then in the module. Else vice-versa
    this.viewParentLookup   = true;
    // If true, looks for layouts in the parent first, if not found, then in module. Else vice-versa
    this.layoutParentLookup = true;
    this.entryPoint = "cbxmigrations";

    function configure() {
        parseParentSettings();
    }
    private function parseParentSettings(){
        var oConfig = controller.getSetting( "ColdBoxConfig" );
        var configStruct = controller.getConfigSettings();
        var cbxmigrations = oConfig.getPropertyMixin( "cbxmigrations", "variables", structnew() );

        //defaults
        configStruct.cbxmigrations = {
            migrationsDir = "/resources/database/migrations"
        };

        // incorporate settings
        structAppend( configStruct.cbxmigrations, cbxmigrations, true );
    }
    function onLoad(){
        var settingService  =   wirebox.getInstance("settingService@cb");
            menuService     =   controller.getWireBox().getInstance("AdminMenuService@cb");

        menuService.addSubMenu(
                            topMenu     =   "system",
                            name        =   "cbxmigrations",
                            label       =   "<i class='fa fa-database'></i> Migrations",
                            permissions =   "MODULES_ADMIN", 
                            href        =   "cbadmin/module/cbxmigrations/main/list");
    }

    /**
    * Fired when the module is unregistered and unloaded
    */
    function onUnload(){
        var menuService     =   controller.getWireBox().getInstance("AdminMenuService@cb");
            menuService.removeSubMenu(topMenu = "system", name="cbxmigrations");
    }
}
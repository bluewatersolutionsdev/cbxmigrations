component {
    property name="securityService" inject="id:securityService@cb";
    property name="authorService"   inject="id:authorService@cb";
    property name='MessageBox' inject='MessageBox@cbMessageBox';
    property name="moduleService"   inject="id:moduleService@cb";
    property migrationService inject="MigrationService@cbxmigrations";
    property config inject="coldbox:setting:cbxmigrations";


/*******************************************************
            PERMISSIONS
********************************************************/
    defaultEventRedirect = "cbadmin/dashboard";
    eventPermissions = {
        "index": {
            "permissions"     : "MODULES_ADMIN",
            "message"     : "You do not have permision to view this."   ,
            "nextEvent"       : "cbadmin/dashboard"
        },
        "install": {
            "permissions"     : "MODULES_ADMIN",
            "message"     : "You do not have permision to view this."   ,
            "nextEvent"       : "cbadmin/dashboard"
        },
        "uninstall": {
            "permissions"     : "MODULES_ADMIN",
            "message"     : "You do not have permision to view this."   ,
            "nextEvent"       : "cbadmin/dashboard"
        },
        "migrate": {
            "permissions"     : "MODULES_ADMIN",
            "message"     : "You do not have permision to view this."   ,
            "nextEvent"       : "cbadmin/dashboard"
        },
        "refresh": {
            "permissions"     : "MODULES_ADMIN",
            "message"     : "You do not have permision to view this."   ,
            "nextEvent"       : "cbadmin/dashboard"
        }




    };


/*******************************************************
            CHECKEVENTPERMISSIONS
********************************************************/
    function checkEventPermissions( event, rc, prc ){
        var currentAction = event.getMemento().context.moduleAction;
        if( structKeyExists( eventPermissions, currentAction ) ){
            if( !prc.oCurrentAuthor.checkPermission( eventPermissions[ currentAction ][ "permissions" ] ) ){
                if( structKeyExists( eventPermissions[ currentAction ], "permissions" ) ){
                    messagebox.error( eventPermissions[ currentAction ][ "message" ] );   
                }
                if( structKeyExists( eventPermissions[ currentAction ], "nextEvent" ) ){
                    setNextEvent( eventPermissions[ currentAction ][ "nextEvent" ] ); 
                } else {
                    setNextEvent( defaultEventRedirect );
                }
            }   
        }
    }
/*******************************************************
            PREHANDLER
********************************************************/
function preHandler( event, rc, prc ){
checkEventPermissions( event, rc, prc, eventPermissions, defaultEventRedirect ); 
}




    function index( event, rc, prc ) {
     
            config.migrationsDir = "/modules/contentbox/modules_user/#rc.mymodule#/resources/database/migrations"
        

       
        prc.migrationTableInstalled = migrationService.isMigrationTableInstalled();
        prc.migrations = migrationService.findAll("myModule"=rc.myModule);
       //writeDump(expandPath( config.migrationsDir ));abort;

        event.setView("main/index");  
       
    }
    function list( event, rc, prc ) {
        var modules = moduleService.findModules();
        prc.modules = modules.modules;
        //writedump(prc.modules);
       // prc.migrationTableInstalled = migrationService.isMigrationTableInstalled();
       // prc.migrations = migrationService.findAll();
      // writeDump(expandPath( config.migrationsDir ));abort;

        event.setView("main/list");  
       
    }

    function install( event, rc, prc ) {
        param rc.runAll = false;
        migrationService.install( rc.runAll );
       setNextEvent( "cbadmin.module.cbxmigrations.main.index.mymodule.#rc.mymodule#" );
    }

    function uninstall( event, rc, prc ) {
        migrationService.uninstall();
        setNextEvent( "cbadmin.module.cbxmigrations.main.index.mymodule.#rc.mymodule#" );
    }

    function migrate( event, rc, prc ) {
        param rc.all = false;
        param rc.next = false;

        if ( rc.all ) {
            migrationService.runAllMigrations( rc.direction );
            setNextEvent( "cbadmin.module.cbxmigrations.main.index.mymodule.#rc.mymodule#" );
            return;
        }

        if ( rc.next ) {
            migrationService.runNextMigration( rc.direction );
            setNextEvent( "cbadmin.module.cbxmigrations.main.index.mymodule.#rc.mymodule#" );
            return;
        }

        if ( ! event.valueExists( "componentPath" ) ) {
            setNextEvent( "cbadmin.module.cbxmigrations.main.index.mymodule.#rc.mymodule#" );
            return;
        }

        try {
            migrationService.runMigration( rc.direction, rc.componentPath );

           setNextEvent( "cbadmin.module.cbxmigrations.main.index.mymodule.#rc.mymodule#" );
            return
        }
        catch ( any e ) {

           setNextEvent( "cbadmin.module.cbxmigrations.main.index.mymodule.#rc.mymodule#" );
            return;
        }
    }

    function refresh( event, rc, prc ) {
        migrationService.runAllMigrations( "down" );
        migrationService.runAllMigrations( "up" );
        setNextEvent( "cbadmin.module.cbxmigrations.main.index.mymodule.#rc.mymodule#" );
        return;
    }

}